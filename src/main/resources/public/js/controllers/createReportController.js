
angular.module('myApp').controller('createReportController',['$scope', '$http', 'createReportFactory', function($scope, $http, createReportFactory){
	$scope.report = {};
	$scope.selectedProject = {};
	$scope.report.reportName = "";
    $scope.getCurrentUser = createReportFactory.getCurrentUser().then(
    		function(success){
    			$scope.currentUser = success.data;
    		},
    		function(error){
    			$scope.currentUser = error;
    	});

    $scope.submitReport = function(report, selectedProject){
    		report.user = $scope.currentUser;
            report.project = selectedProject;
            report.status = {statusName: "submitted"};
            if(report.reportName.length > 2) {
            	createReportFactory.editReport(report).then(
                    function(success){
                        $scope.createReportResult = success;
                        window.location = "/#/home";
                    },
                    function(error){
                        $scope.createReportResult = error;
                    });
            }
            else {
            	console.log("Name didn't meet requirements");
            }
        }
    
    $scope.showSubmitBtn = false;
    $scope.createReport = function(report, selectedProject){
		report.user = $scope.currentUser;
        report.project = selectedProject;
        report.status = {statusName: "saved"};
        if(report.reportName.length > 2) {
    		createReportFactory.createReport(report).then(
                    function(success){
                    	$scope.showSubmitBtn = true;
                        $scope.createReportResult = success;
                    },
                    function(error){
                        $scope.createReportResult = error;
                    });
            }
            else {
            	console.log("Name didn't meet requirements");
            }
        }
    
	$scope.ProjectList = null;
	$scope.fillProjectList = function() {
		$http({
			method: 'GET',
			url: '/project/get',
			data: {}
		}).success(function(result) {
			$scope.ProjectList = result;
		});
	}
	$scope.fillProjectList();
}])
    
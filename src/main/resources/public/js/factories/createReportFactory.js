angular.module('myApp').factory('createReportFactory',['$http', function($http){
    return{
    	createReport: function(report){
            return $http.post('reports', report);
        },
    	getCurrentUser: function(){
    		return $http.get("user/getCurrent")
    	},
    	editReport: function(report){
            return $http.put('reports/id', report);
        }
        
    }	
}]);